import React from 'react'
import { Container,Table,Button,Badge } from 'react-bootstrap'

function TableData(props) {
     let{items,onReset} ={...props}
    let temp=items.filter(item=> {
        return item.amount>0
    })
    return (
        <Container>
            <Button
            onClick={onReset}
             variant="info"
             className="my-2"
            >Reset
            </Button>
             <Badge
             variant="warning"
             className="mx-3 "
            >{temp.length} count</Badge>
            
            <Table striped bordered hover>
  <thead >
    <tr>
      <th>#</th>
      <th>Food</th>
      <th>Amount</th>
      <th>Price</th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody>
      {temp.map((item,index) => (
        <tr>
      <td>{index+1}</td>
      <td>{item.title}</td>
      <td>{item.amount}</td>
      <td>{item.price}</td>
      <td>{item.total}</td>
    </tr>
    
      ))}
    
  </tbody>
</Table>
        </Container>
    )
}

export default TableData
